import pytest
from ruamel_yaml import YAML
from starlette.config import environ

# This line would raise an error if we use it after 'settings' has been imported.
environ["TESTING"] = "TRUE"
environ["GL_SECRET"] = "12345"
environ["EXCLUDE_RECIPE_MR_PROJECTS"] = "my-namespace/require"


@pytest.fixture(scope="function")
def conda_build_config_yaml():
    content = """c_compiler:
  # modern compilers
  - gcc                        # [linux64]
c_compiler_version:
  - 7                          # [linux64]
cxx_compiler:
  # modern compilers
  - gxx                        # [linux64]
cxx_compiler_version:
  - 7                          # [linux64]

# pin_run_as_build is only needed for packages that don't
# define run_exports
# The following can be removed when run_exports is added
pin_run_as_build:
  boost:
    max_pin: x.x.x
  boost-cpp:
    max_pin: x.x.x

# Pinning packages
asyn:
  - 4.33.0
boost:
  - 1.70.0
boost_cpp:
  - 1.70.0
bzip2:
  - 1
calc:
  - 3.7.1
epics_base:
  - 3.15.6
  - 7.0.2.1
require:
  - 3.1.0
seq:
  - 2.1.21
  - 2.2.6
sis8300:
  - 2.5.0

# Link each seq version with one base only
# 2.1 with 3.15
# 2.2 with 7
zip_keys:
  - epics_base
  - seq
"""
    yaml = YAML(typ="rt")
    return yaml.load(content)
