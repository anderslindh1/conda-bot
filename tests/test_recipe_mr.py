import pytest
from gidgetlab import sansio
from gidgetlab_cli.models import Project

from conda_bot import recipe_mr

from .utils import FakeGitLab


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "name, namespace, skip",
    [
        ("foo", "test-group", False),
        ("foo", "my-namespace", False),
        ("require", "another-namespace", False),
        ("require", "my-namespace", True),
    ],
)
async def test_create_recipe_mr(mocker, name, namespace, skip):
    mock = mocker.patch("conda_bot.recipe_mr.create_recipe_merge_request")
    gl = FakeGitLab()
    project = Project(
        name=name, namespace=namespace, path_with_namespace=f"{namespace}/{name}"
    )
    user_id = 42
    tag = "v2.4.0"
    data = {
        "user_id": user_id,
        "ref": f"refs/tags/{tag}",
        "after": "8b9f3881cae5387dc969e66a03515cfe099cf088",
        "project": {
            "name": project.name,
            "namespace": project.namespace,
            "path_with_namespace": project.path_with_namespace,
        },
    }
    event = sansio.Event(data, event="Tag Push Hook")
    await recipe_mr.router.dispatch(event, gl)
    if skip:
        # my-namespace/require is excluded in conftest.py
        mock.assert_not_awaited()
    else:
        mock.assert_awaited_with(gl, project, tag, event.data["user_id"])


@pytest.mark.asyncio
async def test_create_recipe_mr_not_called_on_delete_tag(mocker):
    mock = mocker.patch("conda_bot.recipe_mr.create_recipe_merge_request")
    gl = FakeGitLab()
    name = "foo"
    namespace = "test-group"
    user_id = 42
    tag = "v2.4.0"
    data = {
        "user_id": user_id,
        "ref": f"refs/tags/{tag}",
        "after": "0000000000000000000000000000000000000000",
        "project": {"path_with_namespace": f"{namespace}/{name}"},
    }
    event = sansio.Event(data, event="Tag Push Hook")
    await recipe_mr.router.dispatch(event, gl)
    mock.assert_not_awaited()
