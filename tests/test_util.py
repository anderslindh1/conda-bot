from pathlib import Path

import httpx
import networkx
import pytest
import respx
from gidgetlab_cli.models import Project
from ruamel_yaml import YAML

from conda_bot import exceptions, util

from .utils import FakeGitLab

TEST_REPO = Path(__file__).parent.resolve() / "data" / "conda_repo"
TEST_CHANNEL = f"file://{TEST_REPO}"

RECIPE_CONTENT = """
{% set version = "4.6.1" %}

package:
  name: sis8300drv
  version: "{{ version }}"

source:
  - url: "https://gitlab.esss.lu.se/epics-modules/sis8300drv/-/archive/v{{ version }}/sis8300drv-v{{ version }}.tar.gz"
    sha256: 50f765cc67d76c8660720a70a7bf6efe31b1a48a4b173ce49c40665143185120
  - path: ../src


build:
  number: 0
  run_exports:
    - {{ pin_subpackage('sis8300drv', max_pin='x.x.x') }}

requirements:
  build:
    - {{ compiler('c') }}
    - {{ compiler('cxx') }}
  host:
    - epics-base
    - require
"""
RECIPE_CONTENT_NAME_VAR = """
{% set name = "iocStats" %}
{% set version = "3.1.15.post1" %}

package:
  name: {{ name|lower }}
  version: {{ version }}
"""
RECIPE_CONTENT_NO_VERSION_VAR = """
package:
  name: nds
  version: 2.3.3
"""
RECIPE_CONTENT_MAX_PIN = """
{% set version = "2.2.6" %}

package:
  name: seq
  version: "{{ version }}"

build:
  number: 1
  # Using max_pin='x.x' should be fine according to
  # https://www-csr.bessy.de/control/SoftDist/sequencer-2-1/Introduction.html#versioning-policy
  run_exports:
    - {{ pin_subpackage('seq', max_pin='x.x') }}
"""
RECIPE_CONTENT_MAX_PIN_DOUBLE_QUOTE = """
{% set version = "2.2.6" %}

package:
  name: seq
  version: "{{ version }}"

build:
  number: 1
  # Using max_pin="x.x" should be fine according to
  # https://www-csr.bessy.de/control/SoftDist/sequencer-2-1/Introduction.html#versioning-policy
  run_exports:
    - {{ pin_subpackage("seq", max_pin="x.x") }}
"""


def test_make_graph():
    graph = util.make_graph(TEST_CHANNEL, "linux-64")
    assert sorted(graph.nodes) == [
        "asyn",
        "autosave",
        "binutils",
        "boost",
        "busy",
        "c-compiler",
        "calc",
        "caputlog",
        "cxx-compiler",
        "delaygen",
        "e3-common",
        "e3-compilers",
        "e3-pinning",
        "e3-v4",
        "epics-base",
        "ess",
        "iocstats",
        "ip",
        "ipmicomm",
        "libgcc-ng",
        "libstdcxx-ng",
        "libusb",
        "loki",
        "mcoreutils",
        "modbus",
        "normativetypes",
        "numpy",
        "pcre",
        "perl",
        "pva2pva",
        "pvaccess",
        "pvaclient",
        "pvapy",
        "pvdata",
        "pvdatabase",
        "pyepics",
        "pyparsing",
        "python",
        "re2c",
        "readline",
        "recsync",
        "require",
        "run-iocsh",
        "s7plc",
        "seq",
        "sscan",
        "std",
        "streamdevice",
        "tclx",
        "tk",
    ]
    assert sorted(graph.in_edges("asyn")) == [
        ("busy", "asyn"),
        ("delaygen", "asyn"),
        ("e3-common", "asyn"),
        ("ip", "asyn"),
        ("ipmicomm", "asyn"),
        ("modbus", "asyn"),
        ("std", "asyn"),
        ("streamdevice", "asyn"),
    ]
    assert sorted(graph.out_edges("asyn")) == [
        ("asyn", "epics-base"),
        ("asyn", "libgcc-ng"),
        ("asyn", "libstdcxx-ng"),
        ("asyn", "libusb"),
        ("asyn", "require"),
    ]


def test_get_direct_reverse_dependencies():
    dep = util.get_direct_reverse_dependencies("seq", TEST_CHANNEL, "linux-64")
    assert dep == ["ip", "sscan", "std"]


def test_get_direct_reverse_dependencies_with_ancestors(mocker):
    graph = networkx.DiGraph()
    graph.add_nodes_from(["A", "B", "C", "D", "E", "F", "G", "H"])
    graph.add_edges_from(
        [
            ("B", "A"),
            ("C", "A"),
            ("D", "A"),
            ("H", "A"),
            ("D", "B"),
            ("E", "C"),
            ("G", "E"),
            ("F", "E"),
            ("H", "G"),
        ]
    )
    #      ┌─────┐
    #      │  H  │
    #      └─┬┬──┘
    #        ││
    #    ┌───┘│
    #    │    │
    #    v    │
    #  ┌───┐  │┌───┐
    #  │ G │  ││ F │
    #  └─┬─┘  │└─┬─┘
    #    │    └┐ │
    #    │ ┌───┼─┘
    #    │ │   │
    #    v v   │
    #  ┌─────┐ │ ┌─────┐
    #  │  E  │ │ │  D  │
    #  └─┬───┘ │ └─┬─┬─┘
    #    │     │   │ │
    #    │     │ ┌─┘ │
    #    │   ┌─┘ │   │
    #    v   │   v   │
    #  ┌───┐ │ ┌───┐ │
    #  │ C │ │ │ B │ │
    #  └─┬─┘ │ └─┬─┘ │
    #    │   │   │┌──┘
    #    └─┐ │   ││
    #      │ │   ││
    #      v v   vv
    #    ┌─────────┐
    #    │    A    │
    #    └─────────┘
    mock_make_graph = mocker.patch("conda_bot.util.make_graph")
    mock_make_graph.return_value = graph
    dep = util.get_direct_reverse_dependencies("A", TEST_CHANNEL, "linux-64")
    # D and H are not in the list because they are ancestors of B and C
    assert dep == ["B", "C"]


@pytest.mark.asyncio
async def test_create_project_trigger():
    project_id = 1042
    description = "my trigger"
    token = "fsjfjfegoe"
    gl = FakeGitLab(post={"id": 1, "description": description, "token": token})
    result = await util.create_project_trigger(gl, project_id, description)
    assert gl.post_url[0] == f"/projects/{project_id}/triggers"
    assert gl.post_data[0] == {"description": description}
    assert result == token


@pytest.mark.asyncio
async def test_get_project_trigger_token_existing_token():
    project_id = 1042
    description = "my trigger"
    token = "fsjfjfegoe"
    gl = FakeGitLab(
        getiter=[
            {"id": 1, "description": "another trigger", "token": "xxxxxx"},
            {"id": 2, "description": description, "token": token},
        ]
    )
    result = await util.get_project_trigger_token(gl, project_id, description)
    assert gl.getiter_url == [f"/projects/{project_id}/triggers"]
    assert gl.post_url == []
    assert result == token


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "getiter", ([], [{"id": 1, "description": "another trigger", "token": "xxxxxx"}])
)
async def test_get_project_trigger_token_create(getiter):
    project_id = 1042
    description = "my trigger"
    token = "fsjfjfegoe"
    gl = FakeGitLab(
        getiter=getiter,
        post={"id": project_id, "description": description, "token": token},
    )
    result = await util.get_project_trigger_token(gl, project_id, description)
    assert gl.getiter_url == [f"/projects/{project_id}/triggers"]
    assert gl.post_url[0] == f"/projects/{project_id}/triggers"
    assert gl.post_data[0] == {"description": description}
    assert result == token


@respx.mock
@pytest.mark.asyncio
async def test_get_archive_sha256():
    project = Project(
        name="foo", namespace="mygroup", web_url="https://gitlab.esss.lu.se/mygroup/foo"
    )
    tag = "v1.0.0"
    url = f"{project.web_url}/-/archive/{tag}/{project.name}-{tag}.tar.gz"
    body = b"test string"
    respx.get(url, content=body, status_code=200)
    # responses.add(responses.GET, url, body=body, status=200)
    sha256 = await util.get_archive_sha256(project, tag)
    assert sha256 == "d5579c46dfcc7f18207013e65b44e4cb4e2c2298f4ac457ba8f82743f31e930b"


@respx.mock
@pytest.mark.asyncio
async def test_get_archive_sha256_404():
    project = Project(
        name="foo", namespace="mygroup", web_url="https://gitlab.esss.lu.se/mygroup/foo"
    )
    tag = "1.0.0"
    url = f"{project.web_url}/-/archive/{tag}/{project.name}-{tag}.tar.gz"
    # responses.add(responses.GET, url, status=404)
    respx.get(url, status_code=404)
    with pytest.raises(httpx.HTTPError):
        await util.get_archive_sha256(project, tag)


def test_update_recipe_meta_yaml():
    content = """{% set version = "2.4.0" %}                                                                                                                                                                     [36/1163]                                                                                                                                                                                                         package:
name: sis8300
version: "{{ version }}"

source:
  - url: https://gitlab.esss.lu.se/epics-modules/sis8300/-/archive/v{{ version }}/sis8300-v{{ version }}.tar.gz
    sha256: 86ff08fdf66911e95ca7b85c4b74a95a3bc48d4410be86fb143c6dabcb0132db
  - path: ../src

build:
  number: 2
  run_exports:
    - {{ pin_subpackage('sis8300', max_pin='x.x.x') }}

requirements:
  build:
    - {{ compiler('c') }}
    - {{ compiler('cxx') }}
  host:
    - epics-base
    - require
    - asyn
    - loki
    - nds
    - libxml2
    - sis8300drv
"""
    result = util.update_recipe_meta_yaml(content, "2.5.0", "123456789")
    assert (
        result
        == """{% set version = "2.5.0" %}                                                                                                                                                                     [36/1163]                                                                                                                                                                                                         package:
name: sis8300
version: "{{ version }}"

source:
  - url: https://gitlab.esss.lu.se/epics-modules/sis8300/-/archive/v{{ version }}/sis8300-v{{ version }}.tar.gz
    sha256: 123456789
  - path: ../src

build:
  number: 0
  run_exports:
    - {{ pin_subpackage('sis8300', max_pin='x.x.x') }}

requirements:
  build:
    - {{ compiler('c') }}
    - {{ compiler('cxx') }}
  host:
    - epics-base
    - require
    - asyn
    - loki
    - nds
    - libxml2
    - sis8300drv
"""
    )


@pytest.mark.parametrize(
    "content,name,version,max_pin",
    [
        (RECIPE_CONTENT, "sis8300drv", "4.6.1", "x.x.x"),
        (RECIPE_CONTENT_NAME_VAR, "iocstats", "3.1.15.post1", None),
        (RECIPE_CONTENT_NO_VERSION_VAR, "nds", "2.3.3", None),
        (RECIPE_CONTENT_MAX_PIN, "seq", "2.2.6", "x.x"),
        (RECIPE_CONTENT_MAX_PIN_DOUBLE_QUOTE, "seq", "2.2.6", "x.x"),
    ],
)
def test_get_package_info(content, name, version, max_pin):
    package = util.get_package_info(content)
    assert package.name == name
    assert package.version == version
    if max_pin is None:
        assert package.max_pin is None
    else:
        assert package.max_pin == max_pin


@pytest.mark.parametrize(
    "version,max_pin,expected",
    [
        ("2.2.6", "x.x.x", "2.2.6"),
        ("2.2.6", "x.x", "2.2"),
        ("2.2.6", "x", "2"),
        ("3.1.15.post1", "x.x.x", "3.1.15"),
        ("1.1.0", None, None),
    ],
)
def test_get_pin_version(version, max_pin, expected):
    pin_version = util.get_pin_version(version, max_pin)
    if expected is None:
        assert pin_version is None
    else:
        assert pin_version == expected


def test_get_package_info_invalid():
    content = """foo"""
    with pytest.raises(exceptions.InvalidRecipe):
        util.get_package_info(content)


def test_update_conda_build_config_new_package(conda_build_config_yaml):
    name = "foo"
    version = "1.0.0"
    assert name not in conda_build_config_yaml
    update = util.update_conda_build_config(conda_build_config_yaml, name, version)
    assert update is True
    assert conda_build_config_yaml[name] == [version]


def test_update_conda_build_config_new_version(conda_build_config_yaml):
    name = "sis8300"
    version = "2.6.0"
    assert conda_build_config_yaml[name] == ["2.5.0"]
    update = util.update_conda_build_config(conda_build_config_yaml, name, version)
    assert update is True
    assert conda_build_config_yaml[name] == [version]


def test_update_conda_build_config_existing(conda_build_config_yaml):
    name = "sis8300"
    version = "2.5.0"
    assert conda_build_config_yaml[name] == ["2.5.0"]
    update = util.update_conda_build_config(conda_build_config_yaml, name, version)
    assert update is False
    assert conda_build_config_yaml[name] == ["2.5.0"]


def test_update_conda_build_config_name_with_dash(conda_build_config_yaml):
    name = "epics-base"
    version = "7.0.2.1"
    assert "epics-base" not in conda_build_config_yaml
    assert conda_build_config_yaml["epics_base"] == ["3.15.6", "7.0.2.1"]
    update = util.update_conda_build_config(conda_build_config_yaml, name, version)
    assert update is False
    assert conda_build_config_yaml["epics_base"] == ["3.15.6", "7.0.2.1"]


def test_update_conda_build_config_multiple_versions_existing(conda_build_config_yaml):
    name = "seq"
    version = "2.2.6"
    update = util.update_conda_build_config(conda_build_config_yaml, name, version)
    assert update is False
    assert conda_build_config_yaml[name] == ["2.1.21", "2.2.6"]


def test_update_conda_build_config_multiple_versions_new(conda_build_config_yaml):
    name = "seq"
    version = "2.2.8"
    with pytest.raises(NotImplementedError):
        util.update_conda_build_config(conda_build_config_yaml, name, version)


def test_yaml_to_string():
    content = """c_compiler:
  # modern compilers
  - gcc                        # [linux64]
c_compiler_version:
  - 7                          # [linux64]
cxx_compiler:
  # modern compilers
  - gxx                        # [linux64]
cxx_compiler_version:
  - 7                          # [linux64]

# pin_run_as_build is only needed for packages that don't
# define run_exports
# The following can be removed when run_exports is added
pin_run_as_build:
  boost:
    max_pin: x.x.x
  boost-cpp:
    max_pin: x.x.x

# Pinning packages
asyn:
  - 4.33.0
boost:
  - 1.70.0
boost_cpp:
  - 1.70.0
"""
    yaml = YAML(typ="rt")
    data = yaml.load(content)
    output = util.yaml_to_string(data)
    assert output == content
