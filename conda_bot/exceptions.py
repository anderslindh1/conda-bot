"""Module for exceptions and errors."""


class CondaBotException(Exception):
    """Base exception for this library."""


class InvalidRecipe(CondaBotException):
    """Exception when the recipe couldn't be parsed"""
