# conda-bot

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

The `conda-bot` is a web server that automates the (re)building of conda packages for e3 (EPICS) modules when new versions become available. It does so by responding to webhooks from repositories part of the [e3-recipes] group, and it exists to cover use cases where GitLab CI is insufficient.

It is used to:

- update the global [conda_build_config.yaml](https://gitlab.esss.lu.se/e3-recipes/e3-pinning/-/blob/master/conda_build_config.yaml) file to pin packages to the latest version published to artifactory. Only packages with dependencies are pinned.
- trigger build of dependent recipes
- create a merge request to update the recipe when a tag is pushed to a repository from [epics-modules] group

A blog post documenting the development of this bot can be found [here](https://beenje.github.io/blog/posts/building-a-gitlab-bot-using-gidgetlab-starlette-and-httpx/).

## Configuration

1. Create a webhook in GitLab on the:
   - [e3-recipes] group for **Pipeline** events.
   - [epics-modules] group for **Tag Push** events.
2. Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) to allow the bot user
   to access GitLab API.
3. The following environment variables shall be passed to the bot:
   - `GL_SECRET`: the token secret used when creating the webhook.
   - `GL_ACCESS_TOKEN`: the personal access token to allow operations via the API.

The variable `EXCLUDE_RECIPE_MR_PROJECTS` can be used to exclude some projects to trigger a MR in the recipe on tag push event (comma separated strings of path_with_namespace).

This is done by the [ics-ans-role-conda-bot](https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-conda-bot) Ansible role.

## License

MIT

[e3-recipes]: https://gitlab.esss.lu.se/e3-recipes
[epics-modules]: https://gitlab.esss.lu.se/epics-modules
